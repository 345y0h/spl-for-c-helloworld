# SPL-for-C-HelloWorld #

This is a CLion Project for this [SPL-for-C Project](https://github.com/OTHRegensburgMedieninformatik/SPL-for-C)

### What is this repository for? ###

* Jumpstart for playing with the SPL-for-C Project in Clion
* Help Beginners to learn programming

### How do I get set up? ###

* Import this Project in Clion
* Just grap the SPL-for-C Project form github and link the build forlder in the CMakeLists.txt

